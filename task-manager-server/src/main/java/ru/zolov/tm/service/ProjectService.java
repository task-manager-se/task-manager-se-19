package ru.zolov.tm.service;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.zolov.tm.api.IProjectRepository;
import ru.zolov.tm.api.IProjectService;
import ru.zolov.tm.dto.ProjectDto;
import ru.zolov.tm.entity.Project;
import ru.zolov.tm.entity.Session;
import ru.zolov.tm.entity.User;
import ru.zolov.tm.enumerated.StatusType;
import ru.zolov.tm.exception.AccessForbiddenException;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

@Service
public class ProjectService extends AbstractService<Project> implements IProjectService {

  @Autowired IProjectRepository projectRepository;

  @SneakyThrows public @NotNull Project create(
      @Nullable final User user,
      @Nullable final String name,
      @Nullable final String description,
      @Nullable final String start,
      @Nullable final String finish
  ) {
    if (user == null) throw new EmptyStringException();
    if (name == null || name.isEmpty()) throw new EmptyStringException();
    if (description == null || description.isEmpty()) throw new EmptyStringException();
    if (start == null || start.isEmpty()) throw new EmptyStringException();
    if (finish == null || finish.isEmpty()) throw new EmptyStringException();
    @NotNull final Project project = new Project();
    project.setName(name);
    project.setUser(user);
    project.setDescription(description);
    project.setDateOfStart(dateFormat.parse(start));
    project.setDateOfFinish(dateFormat.parse(finish));
    projectRepository.save(project);
    return project;
  }

  public @NotNull Project findByUserAndId(
      @Nullable final String userId,
      @Nullable final String id
  ) throws EmptyStringException, EmptyRepositoryException {
    if (userId == null || userId.isEmpty()) throw new EmptyStringException();
    if (id == null || id.isEmpty()) throw new EmptyStringException();
    @Nullable Project project = projectRepository.findByUserAndId(userId, id).orElseThrow(EmptyRepositoryException::new);
    return project;
  }

  public @NotNull List<Project> findAllByUserId(
      @Nullable final String userId
  ) throws EmptyStringException, SQLException {
    if (userId == null) throw new EmptyStringException();
    @NotNull List<Project> list = new ArrayList<>();
    list = projectRepository.findByUser(userId);
    return list;
  }

  public @NotNull List<Project> findAll(Session session) throws EmptyStringException, EmptyRepositoryException, AccessForbiddenException, SQLException {
    if (session.getUser().getRole() == null) throw new AccessForbiddenException();
    if (session.getUser().getRole().name() != "ADMIN") throw new AccessForbiddenException();
    @NotNull List<Project> list = new ArrayList<>();
    list = projectRepository.findAll();
    return list;
  }

  public void update(
      @Nullable final User user,
      @Nullable final String id,
      @Nullable final String name,
      @Nullable final String description,
      @Nullable final String start,
      @Nullable final String finish
  ) throws ParseException, EmptyStringException, EmptyRepositoryException, SQLException {
    if (user == null) return;
    if (id == null || id.isEmpty()) return;
    if (name == null || name.isEmpty()) return;
    if (description == null || description.isEmpty()) return;

    @NotNull final Project project = new Project();
    @NotNull final Date startDate = dateFormat.parse(start);
    @NotNull final Date finishDate = dateFormat.parse(finish);
    project.setUser(user);
    project.setId(id);
    project.setName(name);
    project.setDescription(description);
    project.setDateOfStart(startDate);
    project.setDateOfFinish(finishDate);
    project.setStatusType(StatusType.INPROGRESS);
    projectRepository.save(project);
  }

  public void removeById(
      @Nullable final String userId,
      @Nullable final String id
  ) throws EmptyStringException, SQLException, EmptyRepositoryException {
    if (userId == null || userId.isEmpty()) throw new EmptyStringException();
    if (id == null || id.isEmpty()) throw new EmptyStringException();
    @Nullable final Project project = projectRepository.findByUserAndId(userId, id).orElseThrow(EmptyRepositoryException::new);
    projectRepository.delete(project);
  }

  public @NotNull List<Project> findProject(
      @Nullable final String userId,
      @Nullable final String partOfTheName
  ) throws EmptyStringException, SQLException {
    if (userId == null || userId.isEmpty()) throw new EmptyStringException();
    if (partOfTheName == null || partOfTheName.isEmpty()) return projectRepository.findByUser(userId);
    @NotNull List<Project> list = new ArrayList<>();
    list = projectRepository.findProjectsByPartOfThName(userId, partOfTheName);
    return list;
  }

  public void load(
      @NotNull Session session,
      @Nullable List<Project> list
  ) throws EmptyRepositoryException, EmptyStringException, SQLException, AccessForbiddenException {
    if (session.getUser().getRole() == null) throw new AccessForbiddenException();
    if (session.getUser().getRole().name() != "ADMIN") throw new AccessForbiddenException();
    if (list == null) throw new EmptyRepositoryException();
    projectRepository.saveAll(list);
  }

  @Override @NotNull public ProjectDto transformProjectToDto(@NotNull final Project project) {
    @NotNull final ProjectDto projectDto = new ProjectDto();
    projectDto.setId(project.getId());
    projectDto.setName(project.getName());
    projectDto.setDescription(project.getDescription());
    projectDto.setUserId(project.getUser().getId());
    projectDto.setDateOfFinish(project.getDateOfCreate());
    projectDto.setDateOfStart(project.getDateOfStart());
    projectDto.setDateOfFinish(project.getDateOfFinish());
    projectDto.setStatus(project.getStatusType());
    return projectDto;
  }

  @Override @NotNull public Project transformDtoToProject(
      @NotNull final ProjectDto projectDto,
      @NotNull final User user
  ) {
    @NotNull final Project project = new Project();
    project.setId(projectDto.getId());
    project.setName(projectDto.getName());
    project.setDescription(projectDto.getDescription());
    project.setUser(user);
    project.setDateOfCreate(projectDto.getDateOfCreate());
    project.setDateOfStart(projectDto.getDateOfStart());
    project.setDateOfFinish(projectDto.getDateOfFinish());
    project.setStatusType(projectDto.getStatus());
    return project;
  }

  @Override public List<ProjectDto> transformListProject(@NotNull List<Project> projectList) {
    @NotNull final List<ProjectDto> projectDtoList = new ArrayList<>();
    for (@NotNull final Project project : projectList) {
      projectDtoList.add(transformProjectToDto(project));
    }
    return projectDtoList;
  }
}
