package ru.zolov.tm.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.zolov.tm.api.IUserRepository;
import ru.zolov.tm.api.IUserService;
import ru.zolov.tm.dto.UserDto;
import ru.zolov.tm.entity.Session;
import ru.zolov.tm.entity.User;
import ru.zolov.tm.enumerated.RoleType;
import ru.zolov.tm.exception.AccessForbiddenException;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;
import ru.zolov.tm.exception.UserExistException;
import ru.zolov.tm.exception.UserNotFoundException;
import ru.zolov.tm.util.HashUtil;

@Service
public class UserService extends AbstractService<User> implements IUserService {

  @Autowired private IUserRepository userRepository;

  @Override public User login(
      @Nullable final String login,
      @Nullable final String password
  ) throws EmptyStringException, UserNotFoundException {
    if (login == null || login.isEmpty()) throw new EmptyStringException();
    if (password == null || password.isEmpty()) throw new EmptyStringException();
    @Nullable User user = userRepository.findByLogin(login);
    final String passwordHash = HashUtil.md5(password);
    if (passwordHash == null) throw new UserNotFoundException();
    if (user == null) throw new UserNotFoundException();
    if (!passwordHash.equals(user.getPasswordHash())) throw new UserNotFoundException();
    return user;
  }

  @Override public @NotNull String userRegistration(
      @NotNull final String login,
      @NotNull final String password
  ) throws EmptyStringException, UserExistException {
    @Nullable User foundedOne = userRepository.findByLogin(login);
    if (foundedOne != null) throw new UserExistException();
    @NotNull final User user = new User();
    user.setLogin(login);
    user.setRole(RoleType.USER);
    @NotNull final String hashedPassword = HashUtil.md5(password);
    user.setPasswordHash(hashedPassword);
    userRepository.save(user);
    @NotNull final String userId = user.getId();
    return userId;
  }

  public @NotNull List<User> findAll(@NotNull final Session session) throws EmptyRepositoryException, EmptyStringException, SQLException, AccessForbiddenException {
    if (session.getUser().getRole() == null) throw new AccessForbiddenException();
    if (session.getUser().getRole().name() != "ADMIN") throw new AccessForbiddenException();
    return userRepository.findAll();
  }

  @Override public @Nullable User findOneById(@NotNull String id) throws UserNotFoundException {
    @Nullable User user = userRepository.findById(id).orElseThrow(UserNotFoundException::new);
    return user;
  }


  @Override public void updateUserPassword(
      @NotNull final String id,
      @NotNull final String newPassword
  ) throws EmptyStringException, UserNotFoundException, EmptyRepositoryException, SQLException {
    @Nullable User user = userRepository.findById(id).orElseThrow(UserNotFoundException::new);
    if (user == null) throw new UserNotFoundException();
    String hashedPassword = HashUtil.md5(newPassword);
    if (hashedPassword != null) user.setPasswordHash(hashedPassword);
    userRepository.save(user);
  }


  public void load(
      @NotNull Session session,
      @Nullable List<User> list
  ) throws EmptyRepositoryException, AccessForbiddenException {
    if (session.getUser().getRole() == null) throw new AccessForbiddenException();
    if (session.getUser().getRole().name() != "ADMIN") throw new AccessForbiddenException();
    for (@Nullable final User user : list) {
      if (list != null) throw new EmptyRepositoryException();
      userRepository.save(user);
    }
  }

  @Override public UserDto transformUserToDto(@NotNull final User user) {
    @NotNull final UserDto userDto = new UserDto();
    userDto.setId(user.getId());
    userDto.setLogin(user.getLogin());
    userDto.setPasswordHash(user.getPasswordHash());
    userDto.setRole(user.getRole());
    return userDto;
  }

  @Override public List<UserDto> transformListOfUser(@NotNull final List<User> list) {
    List<UserDto> resultList = new ArrayList<>();
    for (@NotNull User user : list) {
      resultList.add(transformUserToDto(user));
    }
    return resultList;
  }
}
