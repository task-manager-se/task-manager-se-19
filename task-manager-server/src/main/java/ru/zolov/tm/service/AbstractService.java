package ru.zolov.tm.service;

import java.text.SimpleDateFormat;
import java.util.Locale;
import ru.zolov.tm.entity.AbstractEntity;

public abstract class AbstractService<E extends AbstractEntity> {

  public SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH);
}
