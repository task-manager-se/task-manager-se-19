package ru.zolov.tm.service;

import java.sql.SQLException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.zolov.tm.api.ISessionRepository;
import ru.zolov.tm.api.ISessionService;
import ru.zolov.tm.dto.SessionDto;
import ru.zolov.tm.entity.Session;
import ru.zolov.tm.entity.User;
import ru.zolov.tm.exception.AccessForbiddenException;
import ru.zolov.tm.exception.SessionExpiredException;
import ru.zolov.tm.util.PropertyUtil;
import ru.zolov.tm.util.SignatureUtil;

@Service
public class SessionService implements ISessionService {
    @Autowired
    private ISessionRepository sessionRepository;

  @NotNull @Override public Session open(final User user) {
    @NotNull final Session session = new Session();
    @NotNull final String salt = PropertyUtil.SALT;
    @NotNull final Integer cycle = PropertyUtil.CYCLE;
    session.setUser(user);
    session.setTimestamp(System.currentTimeMillis());
    session.setSignature(SignatureUtil.sign(session, salt, cycle));
    sessionRepository.save(session);
    return session;
  }

  @Override public void validate(Session session) throws AccessForbiddenException, CloneNotSupportedException, SessionExpiredException {
    @NotNull final String salt = PropertyUtil.SALT;
    @NotNull final Integer cycle = PropertyUtil.CYCLE;
    if (session == null) throw new AccessForbiddenException();
    if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessForbiddenException();
    if (session.getTimestamp() == null) throw new SessionExpiredException();
    if (session.getUser() == null) throw new AccessForbiddenException();
    @NotNull final Session temp = session.clone();
    if (temp == null) throw new AccessForbiddenException();
    @NotNull final String signatureSource = session.getSignature();
    temp.setSignature(null);
    @Nullable final String signatureTarget = SignatureUtil.sign(temp, salt, cycle);
    if (signatureTarget == null) throw new AccessForbiddenException();
    final boolean check = signatureSource.equals(signatureTarget);
    final boolean live = checkSessionIsAlive(session.getTimestamp());
    if (!check) throw new AccessForbiddenException();
    if (!live) throw new SessionExpiredException();
  }

  @Override public void close(Session session) throws SQLException {
    sessionRepository.delete(session);
  }

  @Override public SessionDto transformSessionToDto(
      @NotNull final Session session,
      @NotNull final User user
  ) {
    @NotNull SessionDto sessionDto = new SessionDto();
    sessionDto.setId(session.getId());
    sessionDto.setSignature(session.getSignature());
    sessionDto.setTimestamp(session.getTimestamp());
    sessionDto.setUserId(session.getUser().getId());
    sessionDto.setRole(session.getUser().getRole());
    return sessionDto;
  }

  @Override public Session transformDtoToSession(
      @NotNull SessionDto sessionDto,
      @NotNull User user
  ) {
    @NotNull Session session = new Session();
    session.setId(sessionDto.getId());
    session.setSignature(sessionDto.getSignature());
    session.setTimestamp(sessionDto.getTimestamp());
    session.setUser(user);
    session.getUser().setRole(user.getRole());
    return session;
  }

  public boolean checkSessionIsAlive(@NotNull final Long sessionTimeStamp) {
    @NotNull final long sessionLiveTimeMs = PropertyUtil.SESSION_LIFETIME;
    @NotNull final long currentTimeMs = System.currentTimeMillis();
    return ((currentTimeMs - sessionTimeStamp) <= sessionLiveTimeMs);
  }
}
