package ru.zolov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.zolov.tm.api.IDomainService;
import ru.zolov.tm.api.IProjectService;
import ru.zolov.tm.api.ISessionService;
import ru.zolov.tm.api.ITaskService;
import ru.zolov.tm.api.IUserService;
import ru.zolov.tm.dto.DomainDto;
import ru.zolov.tm.dto.SessionDto;
import ru.zolov.tm.entity.Session;
import ru.zolov.tm.entity.User;

@Service
public class DomainService implements IDomainService {
  @Autowired
  private IProjectService projectService;
  @Autowired
  private ITaskService taskService;
  @Autowired
  private IUserService userService;
  @Autowired
  private ISessionService sessionService;

  @SneakyThrows public void load(@NotNull SessionDto sessionDto, @NotNull final DomainDto domainDto) {
    @NotNull final User user = userService.findOneById(sessionDto.getUserId());
    @NotNull final Session session = sessionService.transformDtoToSession(sessionDto, user);
    projectService.load(session, domainDto.getProject());
    taskService.load(session, domainDto.getTask());
    userService.load(session, domainDto.getUser());

  }

  @SneakyThrows public void save(@NotNull SessionDto sessionDto, @NotNull final DomainDto domainDto) {
    @NotNull final User user = userService.findOneById(sessionDto.getUserId());
    @NotNull final Session session = sessionService.transformDtoToSession(sessionDto, user);
    domainDto.setProject(projectService.findAll(session));
    domainDto.setTask(taskService.findAll(session));
    domainDto.setUser(userService.findAll(session));
  }
}
