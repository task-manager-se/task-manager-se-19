package ru.zolov.tm.service;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.zolov.tm.api.ITaskRepository;
import ru.zolov.tm.api.ITaskService;
import ru.zolov.tm.dto.TaskDto;
import ru.zolov.tm.entity.Project;
import ru.zolov.tm.entity.Session;
import ru.zolov.tm.entity.Task;
import ru.zolov.tm.entity.User;
import ru.zolov.tm.enumerated.StatusType;
import ru.zolov.tm.exception.AccessForbiddenException;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

@Service
public class TaskService extends AbstractService implements ITaskService {
    @Autowired
    private ITaskRepository taskRepository;

    public @NotNull Task create(
            @Nullable final User user,
            @Nullable final Project project,
            @Nullable final String name,
            @Nullable final String description,
            @Nullable final String start,
            @Nullable final String finish
    ) throws EmptyStringException, ParseException {
        if (user == null) throw new EmptyStringException();
        if (project == null) throw new EmptyStringException();
        if (name == null || name.isEmpty()) throw new EmptyStringException();
        if (description == null || description.isEmpty()) throw new EmptyStringException();
        if (start == null || start.isEmpty()) throw new EmptyStringException();
        if (finish == null || finish.isEmpty()) throw new EmptyStringException();
        Task task = new Task();
        task.setUser(user);
        task.setProject(project);
        task.setName(name);
        task.setDescription(description);
        task.setDateOfStart(dateFormat.parse(start));
        task.setDateOfFinish(dateFormat.parse(finish));
        taskRepository.save(task);
        return task;
    }

    public @NotNull List<Task> findAllByUserId(@Nullable String userId) throws EmptyStringException, SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyStringException();
        @NotNull List<Task> list = new ArrayList<>();
        list = taskRepository.findAllByUserId(userId);
        return list;
    }

    public @NotNull List<Task> findAll(@NotNull Session session) throws AccessForbiddenException {
        if (session.getUser().getRole() == null) throw new AccessForbiddenException();
        if (!session.getUser().getRole().equals("ADMIN")) throw new AccessForbiddenException();
        @NotNull List<Task> list = new ArrayList<>();
        list = taskRepository.findAll();
        return list;
    }

    public List<Task> findTaskByProjectId(
            @Nullable final String userId,
            @Nullable final String id
    ) throws EmptyStringException, EmptyRepositoryException, SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyStringException();
        if (id == null || id.isEmpty()) throw new EmptyStringException();
        @NotNull List<Task> list = new ArrayList<>();
        list = taskRepository.findAllByProjectIdUserId(userId, id);
        return list;
    }

    public @Nullable Task findTaskById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws EmptyStringException, EmptyRepositoryException, SQLException {
        if (id == null || id.isEmpty()) throw new EmptyStringException();
        if (userId == null || userId.isEmpty()) throw new EmptyStringException();
        Task task = taskRepository.findOneById(userId, id).orElseThrow(EmptyRepositoryException::new);
        return task;

    }

    public void removeTaskById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws EmptyStringException, EmptyRepositoryException {
        if (userId == null || userId.isEmpty()) throw new EmptyStringException();
        if (id == null || id.isEmpty()) throw new EmptyStringException();
        Task task = taskRepository.findOneById(userId, id).orElseThrow(EmptyRepositoryException::new);
        taskRepository.delete(task);
    }

    public void update(
            @Nullable final User user,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description,
            @Nullable final String start,
            @Nullable final String finish
    ) throws EmptyStringException, ParseException, EmptyRepositoryException {
      if (user == null) throw new EmptyStringException();
      if (id == null || id.isEmpty()) throw new EmptyStringException();
      if (name == null || name.isEmpty()) throw new EmptyStringException();
      @NotNull final Task task = taskRepository.findOneById(user.getId(), id).orElseThrow(EmptyRepositoryException::new);
      task.setId(id);
      task.setUser(user);
      task.setName(name);
      if (description == null || description.isEmpty()) task.setDescription("empty");
      else task.setDescription(description);
      task.setStatusType(StatusType.INPROGRESS);
      task.setName(name);
      @NotNull final Date startDate = dateFormat.parse(start);
      task.setDateOfStart(startDate);
      @NotNull final Date finishDate = dateFormat.parse(finish);
      task.setDateOfFinish(finishDate);
      taskRepository.save(task);
    }

    public @NotNull List<Task> findTask(
            @Nullable final String userId,
            @Nullable final String partOfTheName
    ) throws EmptyRepositoryException, EmptyStringException, SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyStringException();
        if (partOfTheName == null) throw new EmptyStringException();
        @NotNull List<Task> list = new ArrayList<>();
        list = taskRepository.findTaskByPathOfTheName(userId, partOfTheName);
        return list;
    }

    public void load(@NotNull Session session, @Nullable List<Task> list) throws EmptyRepositoryException, SQLException, EmptyStringException, AccessForbiddenException {
        if (list == null) throw new EmptyRepositoryException();
        if (session.getUser().getRole() == null) throw new AccessForbiddenException();
        if (session.getUser().getRole().name() != "ADMIN") throw new AccessForbiddenException();

        for (@Nullable final Task task : list) {
            taskRepository.save(task);
        }
    }

  @Override @NotNull public Task transformDtoToTask(
      @NotNull final TaskDto taskDto,
      @NotNull final User user,
      @NotNull final Project project
  ) {
    @NotNull final Task task = new Task();
    task.setId(taskDto.getId());
    task.setName(taskDto.getName());
    task.setDescription(taskDto.getDescription());
    task.setDateOfCreate(taskDto.getDateOfCreate());
    task.setDateOfStart(taskDto.getDateOfStart());
    task.setDateOfFinish(taskDto.getDateOfFinish());
    task.setStatusType(taskDto.getStatus());
    task.setUser(user);
    task.setProject(project);
        return task;
    }

  @Override @NotNull public TaskDto transformTaskToDto(@NotNull Task task) {
    @NotNull final TaskDto taskDto = new TaskDto();
    taskDto.setId(task.getId());
    if (task.getProject() != null) taskDto.setProjectId(task.getProject().getId());
    if (task.getUser() != null) taskDto.setUserId(task.getUser().getId());
    taskDto.setDescription(task.getDescription());
    taskDto.setDateOfCreate(task.getDateOfCreate());
    taskDto.setDateOfStart(task.getDateOfStart());
    taskDto.setDateOfFinish(task.getDateOfFinish());
    taskDto.setStatus(task.getStatusType());
    return taskDto;
    }

    @Override
    @NotNull
    public List<TaskDto> transformListOfTask(@NotNull List<Task> list) {
        List<TaskDto> listDto = new ArrayList<>();
        for (@NotNull final Task task : list) {
          listDto.add(transformTaskToDto(task));
        }
        return listDto;
    }
}
