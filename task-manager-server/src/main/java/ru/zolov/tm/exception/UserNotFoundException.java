package ru.zolov.tm.exception;

public class UserNotFoundException extends Exception {

  public UserNotFoundException() {
    super("User not found!");
  }
}
