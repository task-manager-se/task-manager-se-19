package ru.zolov.tm.exception;

public class EmptyStringException extends Exception {

  public EmptyStringException() {
    super("Empty string!");
  }
}
