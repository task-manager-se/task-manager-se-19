package ru.zolov.tm.api;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.dto.TaskDto;
import ru.zolov.tm.entity.Project;
import ru.zolov.tm.entity.Session;
import ru.zolov.tm.entity.Task;
import ru.zolov.tm.entity.User;
import ru.zolov.tm.exception.AccessForbiddenException;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

public interface ITaskService {

  @NotNull Task create(
      @Nullable User user,
      @Nullable Project project,
      @Nullable String name,
      @Nullable String description,
      @Nullable String start,
      @Nullable String finish
  ) throws EmptyStringException, EmptyRepositoryException, ParseException, SQLException;

  @NotNull List<Task> findAllByUserId(@Nullable String userId) throws EmptyStringException, EmptyRepositoryException, SQLException;

  @NotNull List<Task> findAll(
      @NotNull Session session
  ) throws EmptyStringException, EmptyRepositoryException, SQLException, AccessForbiddenException;

  List<Task> findTaskByProjectId(
      @Nullable String userId,
      String id
  ) throws EmptyStringException, EmptyRepositoryException, SQLException;

  @Nullable Task findTaskById(
      @Nullable String userId,
      @Nullable String id
  ) throws EmptyStringException, EmptyRepositoryException, SQLException;

  void removeTaskById(
      @Nullable String userId,
      @Nullable String id
  ) throws EmptyStringException, EmptyRepositoryException, SQLException;

  void update(
      @Nullable User user,
      @Nullable String id,
      @Nullable String name,
      @Nullable String description,
      @Nullable String start,
      @Nullable String finish
  ) throws EmptyStringException, EmptyRepositoryException, ParseException, SQLException;

  @NotNull List<Task> findTask(
      @Nullable String userId,
      @Nullable String partOfTheName
  ) throws EmptyRepositoryException, EmptyStringException, SQLException;

  void load(@NotNull Session session, @Nullable final List<Task> list) throws EmptyRepositoryException, SQLException, EmptyStringException, AccessForbiddenException;

  @NotNull Task transformDtoToTask(
      @NotNull TaskDto taskDto,
      @NotNull User user,
      @NotNull Project project
  );

  @NotNull TaskDto transformTaskToDto(@NotNull Task task);

  @NotNull List<TaskDto> transformListOfTask(@NotNull List<Task> list);
}
