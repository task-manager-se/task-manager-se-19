package ru.zolov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.zolov.tm.entity.Project;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
public interface IProjectRepository extends JpaRepository<Project, String> {
  @Query("SELECT p FROM Project p WHERE p.user.id = :userId")
  List<Project> findByUser(@NotNull @Param("userId") String userId);

  @Query("SELECT p FROM Project p WHERE p.id = :id AND p.user.id = :userId")
  Optional<Project> findByUserAndId(@NotNull @Param("id") String id, @NotNull @Param("userId") final String userId);

  @Query("SELECT p FROM Project p WHERE p.user.id = :userId AND (p.name LIKE CONCAT('%',:string, '%') OR p.description LIKE CONCAT('%',:string, '%'))")
  List<Project> findProjectsByPartOfThName(@NotNull @Param("userId") String userId, @NotNull @Param("string") String string);

  @Transactional
  @Modifying
  @Query("DELETE FROM Project p WHERE p.user.id = :userId")
  void removeAllByUserId(@NotNull @Param("userId") String userId);
}
