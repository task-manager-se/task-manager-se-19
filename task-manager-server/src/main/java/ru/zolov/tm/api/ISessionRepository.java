package ru.zolov.tm.api;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.zolov.tm.entity.Session;

@Repository
public interface ISessionRepository extends JpaRepository<Session, String> {
}
