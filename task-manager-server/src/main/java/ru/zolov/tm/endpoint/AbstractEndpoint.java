package ru.zolov.tm.endpoint;

import lombok.Getter;
import org.springframework.stereotype.Component;

@Getter
@Component
public abstract class AbstractEndpoint {

  private final String url = "http://localhost:8080/";
  private final String postfix = "?wsdl";
}
