package ru.zolov.tm.endpoint;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.zolov.tm.api.ISessionService;
import ru.zolov.tm.api.IUserService;
import ru.zolov.tm.dto.SessionDto;
import ru.zolov.tm.entity.Session;
import ru.zolov.tm.entity.User;
import ru.zolov.tm.enumerated.RoleType;
import ru.zolov.tm.exception.AccessForbiddenException;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;
import ru.zolov.tm.exception.UserExistException;


@NoArgsConstructor
@WebService
@Component
public class UserEndpoint extends AbstractEndpoint  {
  @Autowired
  private IUserService userService;
  @Autowired
  private ISessionService sessionService;

  @WebMethod public void registerNewUser(
      @NotNull @WebParam(name = "login") final String login,
      @NotNull @WebParam(name = "password") final String password
  ) throws EmptyStringException, UserExistException, EmptyRepositoryException, SQLException {
     userService.userRegistration(login, password);
  }

  @SneakyThrows @WebMethod public void updateUserPassword(
      @NotNull @WebParam(name = "session") final SessionDto sessionDto,
      @NotNull @WebParam(name = "newPassword") final String newPassword
  ) {
    if (sessionDto.getUserId() == null) throw new AccessForbiddenException();
    @NotNull final User user = userService.findOneById(sessionDto.getUserId());
    @NotNull Session session = sessionService.transformDtoToSession(sessionDto, user);
    sessionService.validate(session);
    userService.updateUserPassword(session.getUser().getId(), newPassword);
  }

  @SneakyThrows @WebMethod public boolean isRolesAllowed(
      @NotNull @WebParam(name = "session") final Session session,
      @Nullable @WebParam(name = "roleType") final RoleType... roleTypes
  ) {
    sessionService.validate(session);
    if (roleTypes == null) return false;
    final List<RoleType> types = Arrays.asList(roleTypes);
    return types.contains(session.getUser().getRole());
  }
}
