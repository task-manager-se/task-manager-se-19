package ru.zolov.tm.endpoint;

import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.zolov.tm.api.IProjectService;
import ru.zolov.tm.api.ISessionService;
import ru.zolov.tm.api.IUserService;
import ru.zolov.tm.dto.ProjectDto;
import ru.zolov.tm.dto.SessionDto;
import ru.zolov.tm.entity.Project;
import ru.zolov.tm.entity.Session;
import ru.zolov.tm.entity.User;


@NoArgsConstructor
@WebService
@Component
public class ProjectEndpoint extends AbstractEndpoint {

  @Autowired private IProjectService projectService;
  @Autowired private ISessionService sessionService;
  @Autowired private IUserService userService;

  @SneakyThrows @NotNull @WebMethod public ProjectDto createNewProject(
      @NotNull @WebParam(name = "session") final SessionDto sessionDto,
      @NotNull @WebParam(name = "name") final String projectName,
      @NotNull @WebParam(name = "description") final String projectDescription,
      @NotNull @WebParam(name = "start") final String dateOfStart,
      @NotNull @WebParam(name = "finish") final String dateOfFinish
  ) {
    @NotNull final User user = userService.findOneById(sessionDto.getUserId());
    @NotNull final Session session = sessionService.transformDtoToSession(sessionDto, user);
    sessionService.validate(session);
    Project project = projectService.create(user, projectName, projectDescription, dateOfStart, dateOfFinish);
    return projectService.transformProjectToDto(project);
  }

  @SneakyThrows @NotNull @WebMethod public List<ProjectDto> findAllProjectByUserId(
      @NotNull @WebParam(name = "session") final SessionDto sessionDto
  ) {
    @NotNull final User user = userService.findOneById(sessionDto.getUserId());
    @NotNull final Session session = sessionService.transformDtoToSession(sessionDto, user);
    sessionService.validate(session);
    List<Project> allByUserId = projectService.findAllByUserId(sessionDto.getUserId());
    return projectService.transformListProject(allByUserId);
  }

  @SneakyThrows @Nullable @WebMethod public ProjectDto findProjectById(
      @NotNull @WebParam(name = "session") final SessionDto sessionDto,
      @NotNull @WebParam(name = "id") String projectId
  ) {
    @NotNull final User user = userService.findOneById(sessionDto.getUserId());
    @NotNull final Session session = sessionService.transformDtoToSession(sessionDto, user);
    sessionService.validate(session);
    Project byUserAndId = projectService.findByUserAndId(sessionDto.getUserId(), projectId);
    return projectService.transformProjectToDto(byUserAndId);
  }

  @SneakyThrows @WebMethod public void removeProjectById(
      @NotNull @WebParam(name = "session") final SessionDto sessionDto,
      @NotNull @WebParam(name = "id") String projectId
  ) {
    @NotNull final User user = userService.findOneById(sessionDto.getUserId());
    @NotNull final Session session = sessionService.transformDtoToSession(sessionDto, user);
    sessionService.validate(session);
    projectService.removeById(sessionDto.getUserId(), projectId);
  }

  @SneakyThrows @WebMethod public void updateProject(
      @NotNull @WebParam(name = "session") final SessionDto sessionDto,
      @NotNull @WebParam(name = "id") final String projectId,
      @NotNull @WebParam(name = "name") final String projectName,
      @NotNull @WebParam(name = "description") final String projectDescription,
      @NotNull @WebParam(name = "start") final String start,
      @NotNull @WebParam(name = "finish") final String finish
  ) {
    @NotNull final User user = userService.findOneById(sessionDto.getUserId());
    @NotNull final Session session = sessionService.transformDtoToSession(sessionDto, user);
    sessionService.validate(session);
    projectService.update(user, projectId, projectName, projectDescription, start, finish);
  }


  @SneakyThrows @NotNull @WebMethod public List<ProjectDto> findProjectByPartOfTheName(
      @NotNull @WebParam(name = "session") final SessionDto sessionDto,
      @NotNull @WebParam(name = "partOfTheName") final String partOfTheName
  ) {
    @NotNull final User user = userService.findOneById(sessionDto.getUserId());
    @NotNull final Session session = sessionService.transformDtoToSession(sessionDto, user);
    sessionService.validate(session);
    List<Project> projects = projectService.findProject(sessionDto.getUserId(), partOfTheName);
    return projectService.transformListProject(projects);
  }
}
