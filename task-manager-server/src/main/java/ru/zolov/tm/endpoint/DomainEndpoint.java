package ru.zolov.tm.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.ser.ToXmlGenerator;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import lombok.Cleanup;
import lombok.NoArgsConstructor;
import org.eclipse.persistence.jaxb.JAXBContextProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.zolov.tm.api.IDomainService;
import ru.zolov.tm.api.ISessionService;
import ru.zolov.tm.api.IUserService;
import ru.zolov.tm.dto.DomainDto;
import ru.zolov.tm.dto.SessionDto;
import ru.zolov.tm.entity.Session;
import ru.zolov.tm.enumerated.PathConstant;
import ru.zolov.tm.exception.AccessForbiddenException;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;
import ru.zolov.tm.exception.SessionExpiredException;

@NoArgsConstructor
@WebService
@Component
public class DomainEndpoint extends AbstractEndpoint {

    @Autowired
    private ISessionService sessionService;
    @Autowired
    private IUserService userService;
    @Autowired
    private IDomainService domainService;

   @WebMethod public void saveToBin(
       @NotNull @WebParam(name = "session") final SessionDto session
  ) throws IOException, EmptyStringException, EmptyRepositoryException, AccessForbiddenException, CloneNotSupportedException, SQLException, SessionExpiredException {
     //    sessionService.validate(session);
    System.out.println("Save data to file.");
    @NotNull final DomainDto domainDto = new DomainDto();
     domainService.save(session, domainDto);
    @Nullable final Path path = Paths.get(PathConstant.BIN.getPath());
    if (path == null) return;
    if (Files.notExists(path.getParent())) Files.createDirectory(path.getParent());
    @Nullable final File file = path.toFile();
    if (file == null) return;
    @Cleanup final FileOutputStream fos = new FileOutputStream(file);
    @Cleanup final ObjectOutputStream oos = new ObjectOutputStream(fos);
    oos.writeObject(domainDto);
    System.out.println("DONE!");
  }

   @WebMethod public void loadFromBin(
       @NotNull @WebParam(name = "session") final SessionDto session
  ) throws IOException, ClassNotFoundException, EmptyStringException, EmptyRepositoryException, AccessForbiddenException, CloneNotSupportedException, SQLException, SessionExpiredException {
    System.out.println("Loading from file ");
     //    sessionService.validate(session);
    @Nullable final File file = new File(PathConstant.BIN.getPath());
    @Cleanup final FileInputStream fis = new FileInputStream(file);
    @Cleanup final ObjectInputStream ois = new ObjectInputStream(fis);
    @NotNull final DomainDto domainDto = (DomainDto)ois.readObject();
//    domainService.load(session, domainDto);
    System.out.println("DONE!");
  }

   @WebMethod public void saveToJsonJackson(
      @NotNull @WebParam(name = "session") final Session session
  ) throws EmptyStringException, EmptyRepositoryException, IOException, AccessForbiddenException, CloneNotSupportedException, SQLException, SessionExpiredException {
    sessionService.validate(session);
    System.out.println("Save data to json file (jackson)");
    @NotNull final DomainDto domainDto = new DomainDto();
//    domainService.save(session, domainDto);
    @Nullable final Path path = Paths.get(PathConstant.JSON.getPath());
    if (path == null) return;
    if (Files.notExists(path.getParent())) Files.createDirectory(path.getParent());
    @Nullable final File file = path.toFile();
    if (file == null) return;
    @NotNull final ObjectMapper objectMapper = new ObjectMapper();
    @NotNull final ObjectWriter objectWriter = objectMapper.writerWithDefaultPrettyPrinter();
    objectWriter.writeValue(file, domainDto);
    System.out.println("Data saved to " + file.getPath());
  }

   @WebMethod public void loadFromJsonJackson(
      @NotNull @WebParam(name = "session") final SessionDto sessionDto
  ) throws IOException, EmptyStringException, EmptyRepositoryException, AccessForbiddenException, CloneNotSupportedException, SQLException, SessionExpiredException {
//    serviceLocator.getSessionService().validate(sessionDto);
    System.out.println("Load data from json file (jackson)");
    @NotNull final File file = new File(PathConstant.JSON.getPath());
    @NotNull final ObjectMapper objectMapper = new ObjectMapper();
    @Nullable final DomainDto domainDto = objectMapper.readValue(file, DomainDto.class);
    if (domainDto == null) return;
//    serviceLocator.getDomainService().load(sessionDto, domainDto);
    System.out.println("Load data from " + file.getPath());
  }

   @WebMethod public void loadFromJsonJaxb(
      @NotNull @WebParam(name = "session") final SessionDto sessionDto
  ) throws AccessForbiddenException, CloneNotSupportedException, JAXBException, EmptyStringException, EmptyRepositoryException, SQLException, SessionExpiredException {
//    serviceLocator.getSessionService().validate(sessionDto);
    System.out.println("Load data from json file (jaxb)");
    @NotNull final File file = new File(PathConstant.JAXBJSON.getPath());
    Map<String, Object> properties = new HashMap<>(2);
    properties.put(JAXBContextProperties.MEDIA_TYPE, "application/json");
    properties.put(JAXBContextProperties.JSON_INCLUDE_ROOT, Boolean.TRUE);
    @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(new Class[]{DomainDto.class}, properties);
    @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
    @NotNull final DomainDto domainDto = (DomainDto)unmarshaller.unmarshal(file);
//    serviceLocator.getDomainService().load(sessionDto, domainDto);
    System.out.println("Load data from " + file.getPath());
  }

   @WebMethod public void saveToJsonJaxb(
      @NotNull @WebParam(name = "session") final SessionDto sessionDto
  ) throws EmptyStringException, EmptyRepositoryException, IOException, JAXBException, AccessForbiddenException, CloneNotSupportedException, SQLException, SessionExpiredException {
//    serviceLocator.getSessionService().validate(sessionDto);
    System.out.println("Save data to json file (jaxb)");
    @NotNull final DomainDto domainDto = new DomainDto();
//    serviceLocator.getDomainService().save(sessionDto, domainDto);
    @Nullable final Path path = Paths.get(PathConstant.JAXBJSON.getPath());
    if (path == null) return;
    if (Files.notExists(path.getParent())) Files.createDirectory(path.getParent());
    @Nullable File file = path.toFile();
    if (file == null) return;
    @NotNull final FileOutputStream fos = new FileOutputStream(file);
    @NotNull final Map<String, Object> properties = new HashMap<>();
    properties.put(JAXBContextProperties.MEDIA_TYPE, "application/json");
    properties.put(JAXBContextProperties.JSON_INCLUDE_ROOT, Boolean.TRUE);
    @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(new Class[]{DomainDto.class}, properties);
    @NotNull final Marshaller marshaller = jaxbContext.createMarshaller();
    marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
    marshaller.marshal(domainDto, fos);
    System.out.println("Data saved to " + file.getPath());
  }

   @WebMethod public void saveToXmlJaxb(
      @NotNull @WebParam(name = "session") final SessionDto sessionDto
  ) throws AccessForbiddenException, CloneNotSupportedException, EmptyStringException, EmptyRepositoryException, IOException, JAXBException, SQLException, SessionExpiredException {
//    serviceLocator.getSessionService().validate(sessionDto);
    System.out.println("Save data to xml file (jaxb)");
    @NotNull final DomainDto domainDto = new DomainDto();
//    serviceLocator.getDomainService().save(sessionDto, domainDto);
    @Nullable final Path path = Paths.get(PathConstant.XML.getPath());
    if (path == null) return;
    if (Files.notExists(path.getParent())) Files.createDirectory(path.getParent());
    @Nullable final File file = path.toFile();
    if (file == null) return;
    @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(DomainDto.class);
    @NotNull final Marshaller marshaller = jaxbContext.createMarshaller();
    marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
    marshaller.marshal(domainDto, file);
    System.out.println("Data saved to " + file.getPath());
  }

   @WebMethod public void loadFromXmlJaxb(
      @NotNull @WebParam(name = "session") final SessionDto sessionDto
  ) throws EmptyStringException, EmptyRepositoryException, AccessForbiddenException, CloneNotSupportedException, JAXBException, SQLException, SessionExpiredException {
//    serviceLocator.getSessionService().validate(sessionDto);
    System.out.println("Load data from xml file (jaxb)");
    @NotNull final File file = new File(PathConstant.XML.getPath());
    @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(DomainDto.class);
    @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
    @NotNull final DomainDto domainDto = (DomainDto)unmarshaller.unmarshal(file);
//    serviceLocator.getDomainService().load(sessionDto, domainDto);
    System.out.println("Data loaded from " + file.getPath());
  }

   @WebMethod public void saveToXmlJackson(
      @NotNull @WebParam(name = "session") final SessionDto sessionDto
  ) throws IOException, EmptyStringException, EmptyRepositoryException, AccessForbiddenException, CloneNotSupportedException, SQLException, SessionExpiredException {
//    serviceLocator.getSessionService().validate(sessionDto);
    System.out.println("Save data to xml file (jackson)");
    @NotNull final DomainDto domainDto = new DomainDto();
//    serviceLocator.getDomainService().save(sessionDto, domainDto);
    @Nullable final Path path = Paths.get(PathConstant.XML.getPath());
    if (path == null) return;
    if (Files.notExists(path.getParent())) Files.createDirectory(path.getParent());
    @Nullable File file = path.toFile();
    if (file == null) return;
    @NotNull final ObjectMapper objectMapper = new XmlMapper().configure(ToXmlGenerator.Feature.WRITE_XML_1_1, true);
    objectMapper.writerWithDefaultPrettyPrinter().writeValue(file, domainDto);
    System.out.println("Data saved to " + file.getPath());
  }

   @WebMethod public void loadFromXmlJackson(
      @NotNull @WebParam(name = "session") final SessionDto sessionDto
  ) throws IOException, EmptyStringException, EmptyRepositoryException, AccessForbiddenException, CloneNotSupportedException, SQLException, SessionExpiredException {
//    serviceLocator.getSessionService().validate(sessionDto);
    System.out.println("Load data from xml file (jackson)");
    @NotNull final File file = new File(PathConstant.XML.getPath());
    @NotNull final ObjectMapper objectMapper = new XmlMapper();
    @NotNull final DomainDto domainDto = objectMapper.readValue(file, DomainDto.class);
//    serviceLocator.getDomainService().load(sessionDto, domainDto);
    System.out.println("Data load from " + file.getPath());
  }
}

