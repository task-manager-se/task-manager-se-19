package ru.zolov.tm.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.zolov.tm.api.ISessionService;
import ru.zolov.tm.api.IUserService;
import ru.zolov.tm.dto.SessionDto;
import ru.zolov.tm.entity.Session;
import ru.zolov.tm.entity.User;

@NoArgsConstructor
@WebService
@Component
public class SessionEndpoint extends AbstractEndpoint {

  @Autowired private IUserService userService;
  @Autowired private ISessionService sessionService;

  @SneakyThrows @Nullable @WebMethod public SessionDto openSession(
      @NotNull @WebParam(name = "login") String login,
      @NotNull @WebParam(name = "password") String password
  ) {
    @NotNull final User current = userService.login(login, password);
    @NotNull final Session newSession = sessionService.open(current);
    @NotNull final SessionDto newSessionDto = sessionService.transformSessionToDto(newSession, current);

    return newSessionDto;
  }


  @SneakyThrows @WebMethod public void closeSesson(
      @NotNull @WebParam(name = "session") SessionDto sessionDto
  ) {
    @NotNull final User user = userService.findOneById(sessionDto.getUserId());
    @NotNull final Session session = sessionService.transformDtoToSession(sessionDto, user);
    sessionService.close(session);
  }
}
