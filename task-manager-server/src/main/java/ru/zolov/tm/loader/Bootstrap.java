package ru.zolov.tm.loader;

import java.util.LinkedHashSet;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.zolov.tm.endpoint.AbstractEndpoint;
import ru.zolov.tm.util.PublisherUtil;

@Component
public final class Bootstrap {

  @Autowired
  private final Set<AbstractEndpoint> endpoits = new LinkedHashSet<>();

  public void init() {
    try {
      PublisherUtil.publish(endpoits);
    } catch (Exception e) {
      System.err.println(e.getMessage());
      e.printStackTrace();
    }
  }
}