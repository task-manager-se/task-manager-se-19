package ru.zolov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.zolov.tm.enumerated.RoleType;

@Getter
@Setter
@NoArgsConstructor
public class UserDto extends AbstractEntityDto {

  @NotNull private String login = "";
  @NotNull private String passwordHash = "";
  @NotNull private RoleType role = RoleType.USER;

  @Override public String toString() {
    return "User: " + login + ", RoleType: " + role;
  }
}
