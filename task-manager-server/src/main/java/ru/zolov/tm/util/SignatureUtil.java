package ru.zolov.tm.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class SignatureUtil {

  private SignatureUtil() {
  }

  public static String sign(
      @Nullable final Object value,
      @Nullable final String salt,
      @Nullable final Integer cycle
  ) {
    @NotNull final ObjectMapper objectMapper = new ObjectMapper();
    try {
      @NotNull final String json = objectMapper.writeValueAsString(value);
      return sign(json, salt, cycle);
    } catch (JsonProcessingException e) {
      e.printStackTrace();
    }
    return null;
  }

  @Nullable private static String sign(
      @Nullable final String value,
      @Nullable final String salt,
      @Nullable final Integer cycle
  ) {
    if (value == null || salt == null || cycle == null) return null;
    @Nullable String result = value;
    for (int i = 0; i < cycle; i++) {
      result = HashUtil.md5(salt + result + salt);
    }
    return result;
  }
}
