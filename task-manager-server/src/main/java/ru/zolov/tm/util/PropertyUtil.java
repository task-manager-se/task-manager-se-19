package ru.zolov.tm.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import org.jetbrains.annotations.Nullable;

public class PropertyUtil {

  private PropertyUtil() {
  }

  @Nullable private static Properties properties;

  static {
    try {
      properties = new Properties();
      InputStream inputStream = ClassLoader.class.getResourceAsStream("/application.properties");
      properties.load(inputStream);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
  public static final String MYSQL_PASSWORD = properties.getProperty("mysql.password");
  public static final String MYSQL_USER = properties.getProperty("mysql.username");
  public static final String MYSQL_DRIVER = properties.getProperty("mysql.driver");
  public static final String MYSQL_URL = properties.getProperty("mysql.url");
  public static final String SALT = properties.getProperty("signature.salt");
  public static final Integer CYCLE = Integer.parseInt(properties.getProperty("signature.cycle"));
  public static final Long SESSION_LIFETIME = Long.parseLong(properties.getProperty("session.lifetime"));
}
