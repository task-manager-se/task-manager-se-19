package ru.zolov.tm.util;

import java.util.Scanner;
import ru.zolov.tm.entity.AbstractEntity;
import ru.zolov.tm.service.AbstractService;


public class TerminalUtil extends AbstractService<AbstractEntity> {

  private static final Scanner scanner = new Scanner(System.in);


  public static String nextLine() {
    return scanner.nextLine();
  }


  public static Integer nextInt() {
    return scanner.nextInt();
  }

}