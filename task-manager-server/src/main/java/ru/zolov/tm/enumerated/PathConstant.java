package ru.zolov.tm.enumerated;

import java.io.File;
import org.jetbrains.annotations.NotNull;

public enum PathConstant {
  BIN("." + File.separator + "var" + File.separator + "data.bin"),
  JSON("." + File.separator + "var" + File.separator + "data.json"),
  JAXBJSON("." + File.separator + "var" + File.separator + "jdata.json"),
  XML("." + File.separator + "var" + File.separator + "data.xml");

  private final String path;

  PathConstant(String path) {this.path = path;}

  @NotNull
  public String getPath() {
    return path;
  }

  @Override
  public String toString() {
    return path;
  }
}
