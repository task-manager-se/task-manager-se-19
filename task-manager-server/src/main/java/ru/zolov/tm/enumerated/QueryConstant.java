package ru.zolov.tm.enumerated;

public enum QueryConstant {
  ID,
  DATE_CREATE,
  DATE_START,
  DATE_FINISH,
  DESCRIPTION,
  NAME,
  STATUS,
  USER_ID,
  SIGNATURE,
  TIMESTAMP,
  PROJECT_ID,
  PASSWORD_HASH,
  ROLE,
  LOGIN
}
