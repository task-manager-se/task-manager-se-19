package ru.zolov.tm.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.zolov.tm.enumerated.RoleType;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "app_user")
public class User extends AbstractEntity {

    @NotNull
    @Column(unique = true)
    private String login = "";

  @NotNull
    @Column(name = "password_hash")
    private String passwordHash = "";

  @NotNull
  @Column(name = "role")
  @Enumerated(value = EnumType.STRING)
    private RoleType role = RoleType.USER;
}
