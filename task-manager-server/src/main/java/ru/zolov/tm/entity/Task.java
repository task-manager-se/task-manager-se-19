package ru.zolov.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "app_task")
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Task extends AbstractGoal {
    @Nullable
    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Project project;
}